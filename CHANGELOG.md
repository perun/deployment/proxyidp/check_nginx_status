# 1.0.0 (2023-06-28)


### Bug Fixes

* flake8 violations fixed ([5b4bec2](https://gitlab.ics.muni.cz/perun/deployment/proxyidp/check_nginx_status/commit/5b4bec2906ab40abece80573057e691c9289b82f))
