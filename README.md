# Dogtown-Nagios-Plugins

(c) copyright 2008-2018 dogtown@mare-system.de

Original [plugins](https://bitbucket.org/maresystem/dogtown-nagios-plugins/src) were forked
and the check_nginx_status plugin is modified by Perun team to be compatible with Python version 3.

# Included Plugins

- check_nginx_status - extract and monitor values from nginx_status_page

# License:

- see LICENSE:

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
